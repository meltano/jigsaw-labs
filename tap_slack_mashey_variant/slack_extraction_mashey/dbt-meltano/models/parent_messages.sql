WITH channels AS (
    
	SELECT * FROM {{ ref('focused_channels') }}

), parent_messages AS (
	SELECT
        channel_id,
		(TIMESTAMP 'epoch' + thread_ts * INTERVAL '1 second') AS thread_datetime,
		(TIMESTAMP 'epoch' + ts * INTERVAL '1 second') AS timestamp_datetime,
		reply_count,
		reply_users,
		text
	FROM threads
    AND text NOT LIKE '%has joined the channel%'
	AND text NOT LIKE '%set the channel%'

), final AS (
    SELECT * FROM parent_messages
    JOIN channels ON channels.channel_id = parent_messages.channel_id
    WHERE thread_datetime = timestamp_datetime

)

SELECT * FROM final WHERE final.name IN {{ ref('focused_channels') }}

{{ dbt_audit(
    cte_ref="parent_messages",
    created_by="@waynebanksy",
    updated_by="@waynebanksy",
    created_date="2022-03-13",
    updated_date="2022-03-13"
) }}