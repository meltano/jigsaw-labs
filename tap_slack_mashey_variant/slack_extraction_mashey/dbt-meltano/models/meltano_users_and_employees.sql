WITH meltano_employees AS (
    
    SELECT * FROM {{ ref('meltano_employee_users') }}

), meltano_users AS (
    
    SELECT * FROM {{ ref('meltano_users') }}

), final AS (

    SELECT * FROM meltano_employees

    UNION

    SELECT * FROM meltano_users

)

SELECT * FROM final 

