WITH channels AS (
    
	SELECT * FROM {{ ref('focused_channels') }}

), threaded_messages AS (

	SELECT
		channel_id,
		(TIMESTAMP 'epoch' + thread_ts * INTERVAL '1 second') AS thread_datetime,
		(TIMESTAMP 'epoch' + ts * INTERVAL '1 second') AS timestamp_datetime,
		reply_count,
		reply_users,
		text
	FROM threads
	WHERE text NOT LIKE '%has joined the channel%'
	AND text NOT LIKE '%set the channel%'

), final AS (
	
	SELECT
		channel_name,
		threaded_messages.channel_id AS channel_id,
		threaded_messages.thread_datetime AS thread_datetime,
		threaded_messages.timestamp_datetime AS timestamp_datetime,
		threaded_messages.reply_count AS reply_count,
		threaded_messages.reply_users AS reply_users,
		threaded_messages.text AS text
	FROM threaded_messages
	FULL OUTER JOIN channels ON channels.channel_id = threaded_messages.channel_id

)

SELECT * FROM final
ORDER BY thread_datetime, timestamp_datetime ASC

{{ dbt_audit(
    cte_ref="threaded_messages",
    created_by="@waynebanksy",
    updated_by="@waynebanksy",
    created_date="2022-03-13",
    updated_date="2022-03-13"
) }}
