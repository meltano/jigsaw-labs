# Jigsaw Labs

## 2022 Project Proposal

**Meltano Slack Analysis**

Use Meltano, and potentially other tools, to do the following:

* Extract all chat logs from *public channels* from the [Meltano Slack instance](https://meltano.com/slack).
* Transform the data into a useful format for categorization
* Categorize conversations in a way that will answers the questions presented below
* Set up the pipeline to run on a regular basis so new conversations can be easily categorized


**Questions to Answer**

* When users ask for help, are they having troubles with Meltano itself or with a plugin (Singer, dbt, etc.)
* How have the Meltano vs plugin percentages changes over time?
* Which plugins are they asking questions about? Tap-postgres, target-snowflake, etc.?


**Other potentially interesting questions**

* What's the average time for someone to respond to a question on Slack?
* What percentage of questions have gone answered?
* What percentage of answered questions are done by non-Meltano team members?


**Workflow**

* For each step of the process, use an [issue](https://gitlab.com/meltano/jigsaw-labs/-/issues) to start the process.
* Open a [Merge Request](https://gitlab.com/meltano/jigsaw-labs/-/merge_requests) from an issue using the `Create merge request` button
* Use the Meltano Slack instance to ask for help. Please use a public channel first before asking in the private #partner-jigsaw-labs channel or asking a Meltano team member.
